﻿using System;
using System.Reflection;
using Application.Interfaces.Repositories;
using Microsoft.Extensions.DependencyInjection;

namespace Application
{
	public static class ServiceExtensions
	{
		public static void AddApplicationLayer(this IServiceCollection services)
		{
			services.AddAutoMapper(Assembly.GetExecutingAssembly());
        }
    }
}

