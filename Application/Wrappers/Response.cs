﻿using System;
namespace Application.Wrappers
{
	public class Response<T>
	{
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public string Code { get; set; }
        public T Result { get; set; }

        public Response() { }

        public Response(T result, string message = null)
        {
            IsSuccess = true;
            Message = message;
            Result = result;
        }

        public Response(string message = null)
        {
            IsSuccess = false;
            Message = message;
        }
    }
}

