﻿using System;
using Domain.Common;
using Domain.Entities;

namespace Application.DTOs
{
	public class TransactionDto : AuditableBaseEntity
    {
        public string Type { get; set; }
        public int Value { get; set; }
        public int Balance { get; set; }
        public int AccountId { get; set; }
        public Account Account { get; set; }
    }
}

