﻿using System;
using System.Collections.Generic;
using Domain.Common;

namespace Application.DTOs
{
	public class ClientDto : Person
	{
        public string Password { get; set; }
        public string Status { get; set; }
        public IEnumerable<AccountDto> Accounts { get; set; }
    }
}

