﻿using System;
using Domain.Common;
using Domain.Entities;

namespace Application.DTOs
{
	public class AccountDto : AuditableBaseEntity
    {
        public string Number { get; set; }
        public string Type { get; set; }
        public int InitialBalance { get; set; }
        public string Status { get; set; }
        public int ClientId { get; set; }
        public ClientDto Client { get; set; }
    }
}

