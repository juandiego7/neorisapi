﻿using System;
using Newtonsoft.Json;

namespace Application.DTOs
{
	public class ReportDto
	{
        [JsonProperty(PropertyName = "Fecha")]
        public string Date { get; set; }
        [JsonProperty(PropertyName = "Cliente")]
        public string Client { get; set; }
        [JsonProperty(PropertyName = "Numero Cuenta")]
        public string AccountNumber { get; set; }
        [JsonProperty(PropertyName = "Tipo")]
        public string AccountType { get; set; }
        [JsonProperty(PropertyName = "Saldo Inicial")]
        public string InitialBalance { get; set; }
        [JsonProperty(PropertyName = "Estado")]
        public string Status { get; set; }
        [JsonProperty(PropertyName = "Movimiento")]
        public string TransactValue { get; set; }
        [JsonProperty(PropertyName = "Saldo Disponible")]
        public string AvailableBalance { get; set; }
    }
}

