﻿using System;
using Application.DTOs;
using Application.Wrappers;
using System.Collections.Generic;
using System.Threading.Tasks;
using Domain.Entities;
using System.Linq;

namespace Application.Interfaces.Repositories
{
	public interface ITransactionRepository : IRepository<Transaction>
    {
        public int GetInitialBalance(int accountId);

        public IEnumerable<Transaction> GetReport(DateTime startDate, DateTime endDate, int clientId);
    }
}

