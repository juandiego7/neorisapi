﻿using System;
using Domain.Entities;
using System.Collections.Generic;

namespace Application.Interfaces.Repositories
{
	public interface IClientRepository : IRepository<Client>
	{
	}
}

