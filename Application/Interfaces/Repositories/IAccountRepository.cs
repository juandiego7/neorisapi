﻿using System;
using Domain.Entities;

namespace Application.Interfaces.Repositories
{
	public interface IAccountRepository : IRepository<Account>
    { 
    }
}

