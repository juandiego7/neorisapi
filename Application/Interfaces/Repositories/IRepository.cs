﻿using System;
using Ardalis.Specification;

namespace Application.Interfaces.Repositories
{
	public interface IRepository <T> : IRepositoryBase<T>, IReadRepositoryBase<T>  where T : class
	{
	}
    //public interface IReadRepository<T> : IReadRepositoryBase<T> where T : class
    //{
    //}
}

