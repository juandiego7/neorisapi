﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Application.DTOs;
using Application.Wrappers;

namespace Application.Interfaces.Services
{
	public interface IClientService
	{
        Task<Response<ClientDto>> Create(ClientDto client);
        Task<Response<bool>> Update(ClientDto client);
        Task<Response<bool>> Delete(int id);
        Task<Response<ClientDto>> GetById(int id);
        Task<Response<IEnumerable<ClientDto>>> Get();
    }
}

