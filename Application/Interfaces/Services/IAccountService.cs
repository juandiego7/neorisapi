﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Application.DTOs;
using Application.Wrappers;

namespace Application.Interfaces.Services
{
	public interface IAccountService
	{
        Task<Response<AccountDto>> Create(AccountDto account);
        Task<Response<bool>> Update(AccountDto account);
        Task<Response<bool>> Delete(int id);
        Task<Response<AccountDto>> GetById(int id);
        Task<Response<IEnumerable<AccountDto>>> Get();
    }
}

