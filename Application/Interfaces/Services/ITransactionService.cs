﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Application.DTOs;
using Application.Wrappers;

namespace Application.Interfaces.Services
{
	public interface ITransactionService
	{
        Task<Response<TransactionDto>> Create(TransactionDto transaction);
        Task<Response<bool>> Update(TransactionDto transaction);
        Task<Response<bool>> Delete(int id);
        Task<Response<TransactionDto>> GetById(int id);
        Task<Response<IEnumerable<TransactionDto>>> Get();
        Response<IEnumerable<ReportDto>> GetReport(string startDate, string endDate, int clientId);
    }
}


