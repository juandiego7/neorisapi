﻿using System;
using Application.DTOs;
using AutoMapper;
using Domain.Entities;

namespace Application.Mappings
{
	public class GeneralProfile : Profile
	{
		public GeneralProfile()
		{
			CreateMap<Client, ClientDto>().ReverseMap();
            CreateMap<Account, AccountDto>().ReverseMap();
            CreateMap<Transaction, TransactionDto>().ReverseMap();
        }
	}
}

