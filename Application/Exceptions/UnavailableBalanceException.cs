﻿using System;
using System.Globalization;

namespace Application.Exceptions
{
    public class UnavailableBalanceException : BadRequestException
    {
        public UnavailableBalanceException() : base("Saldo no disponible")
        {
        }
      
    }
}

