﻿using System;
using Application.Interfaces.Repositories;
using Ardalis.Specification.EntityFrameworkCore;
using Persistence.Context;

namespace Persistence.Repositories
{
	public class Repository<T> : RepositoryBase<T>, IRepository<T> where T : class
    {
		private readonly ApplicationDbContext _dbContext;

		public Repository(ApplicationDbContext  dbContext) : base(dbContext)
		{
            _dbContext = dbContext;
		}
	}
}

