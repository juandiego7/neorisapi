﻿using System;
using System.Collections.Generic;
using Application.Interfaces.Repositories;
using Application.Wrappers;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Persistence.Context;

namespace Persistence.Repositories
{
	public class ClientRepository : Repository<Client>, IClientRepository
	{
        public ClientRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }
    }
}

