﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.DTOs;
using Application.Interfaces.Repositories;
using Application.Wrappers;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Persistence.Context;

namespace Persistence.Repositories
{
	public class TransactionRepository : Repository<Transaction>, ITransactionRepository
    {
        private readonly ApplicationDbContext _dbContext;
        public TransactionRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
        }

        public int GetInitialBalance(int accountId)
        {
            var account = _dbContext.Set<Account>()
                                    .AsNoTracking()
                                    .AsQueryable()
                                    .FirstOrDefault(a => a.Id == accountId);

            return account.InitialBalance;
        }

        public IEnumerable<Transaction> GetReport(DateTime startDate, DateTime endDate, int clientId)
        {
            return _dbContext.Transactions
                             .Include(t => t.Account).ThenInclude(a => a.Client)
                             .Where(t => t.Created >= startDate && t.Created <= endDate && t.Account.ClientId == clientId);
        }
    }
}
