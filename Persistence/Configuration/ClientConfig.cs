﻿using System;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Persistence.Configuration
{
	public class ClientConfig : IEntityTypeConfiguration<Client>
	{
        public void Configure(EntityTypeBuilder<Client> builder)
        {
            builder.ToTable("Clients");

            builder.HasKey(p => p.Id);

            builder.Property(p => p.Name)
                   .HasMaxLength(100)
                   .IsRequired();

            builder.Property(p => p.Gender)
                   .HasMaxLength(15);

            builder.Property(p => p.Age)
                   .HasMaxLength(3);

            builder.Property(p => p.PersonId)
                   .HasMaxLength(15);

            builder.Property(p => p.Address)
                   .HasMaxLength(50)
                   .IsRequired();

            builder.Property(p => p.Telephone)
                   .HasMaxLength(12)
                   .IsRequired();

            builder.Property(p => p.Status)
                   .HasMaxLength(6)
                   .IsRequired();

            builder.Property(p => p.CreatedBy)
                   .HasMaxLength(30);

            builder.Property(p => p.LastModifiedBy)
                   .HasMaxLength(30);

            builder.Property(p => p.Password)
                   .HasMaxLength(100);

        }
    }
}

