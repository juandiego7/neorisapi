﻿using System;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Persistence.Configuration
{
	public class AccountConfig : IEntityTypeConfiguration<Account>
    {
        public void Configure(EntityTypeBuilder<Account> builder)
        {
            builder.ToTable("Accounts");

            builder.HasKey(p => p.Id);

            builder.Property(p => p.Number)
                   .HasMaxLength(15)
                   .IsRequired();

            builder.Property(p => p.Type)
                   .HasMaxLength(10)
                   .IsRequired();

            builder.Property(p => p.InitialBalance)
                   .IsRequired();

            builder.Property(p => p.Status)
                   .HasMaxLength(6)
                   .IsRequired();

            builder.Property(p => p.ClientId)
                   .IsRequired();

            builder.Property(p => p.CreatedBy)
                   .HasMaxLength(30);

            builder.Property(p => p.LastModifiedBy)
                   .HasMaxLength(30);
        }
    }
}

