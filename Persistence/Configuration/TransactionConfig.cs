﻿using System;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Persistence.Configuration
{
	public class TransactionConfig : IEntityTypeConfiguration<Transaction>
    {
        public void Configure(EntityTypeBuilder<Transaction> builder)
        {
            builder.ToTable("Transactions");

            builder.HasKey(p => p.Id);

            builder.Property(p => p.Type)
                   .HasMaxLength(5)
                   .IsRequired();

            builder.Property(p => p.Value)
                   .IsRequired();

            builder.Property(p => p.Balance)
                   .IsRequired();

            builder.Property(p => p.AccountId)
                   .IsRequired();

            builder.Property(p => p.Created)
                   .IsRequired();

            builder.Property(p => p.CreatedBy)
                   .HasMaxLength(30);

            builder.Property(p => p.LastModifiedBy)
                   .HasMaxLength(30);
        }
    }
}

