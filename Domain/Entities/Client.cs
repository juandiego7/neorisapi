﻿using System;
using System.Collections.Generic;
using Domain.Common;

namespace Domain.Entities
{
	public class Client : Person
	{
		public string Password { get; set; }
		public string Status { get; set; }
        public ICollection<Account> Accounts { get; set; }
    }
}

