﻿using System;
using Domain.Common;

namespace Domain.Entities
{
	public class Transaction : AuditableBaseEntity
    {
		public string Type { get; set; }
		public int Value { get; set; }
		public int Balance { get; set; }
		public int AccountId { get; set; }
		public Account Account { get; set; }
	}
}

