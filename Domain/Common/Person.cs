﻿using System;
namespace Domain.Common
{
	public class Person : AuditableBaseEntity
	{
		public string Name { get; set; }
		public string Gender { get; set; }
		public int Age { get; set; }
		public string PersonId { get; set; }
		public string Address { get; set; }
		public string Telephone { get; set; }
	}
}

