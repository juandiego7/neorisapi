﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Application.DTOs;
using Application.Interfaces.Repositories;
using Application.Interfaces.Services;
using Application.Wrappers;
using AutoMapper;
using Domain.Entities;

namespace Services
{
	public class AccountService : IAccountService
	{
        private readonly IAccountRepository _repository;
        private readonly IMapper _mapper;

        public AccountService(IAccountRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<Response<AccountDto>> Create(AccountDto account)
        {
            var accountMap = _mapper.Map<Account>(account);
            var result = await _repository.AddAsync(accountMap);
            var accpuntCreated = _mapper.Map<AccountDto>(result);
            return new Response<AccountDto>(accpuntCreated);
        }

        public async Task<Response<bool>> Delete(int id)
        {
            var account = await _repository.GetByIdAsync(id);
            await _repository.DeleteAsync(account);
            return new Response<bool>(true);
        }

        public async Task<Response<IEnumerable<AccountDto>>> Get()
        {
            var result = await _repository.ListAsync();
            var accounts = _mapper.Map<IEnumerable<AccountDto>>(result);
            return new Response<IEnumerable<AccountDto>>(accounts);
        }

        public async Task<Response<AccountDto>> GetById(int id)
        {
            var result = await _repository.GetByIdAsync(id);
            var account = _mapper.Map<AccountDto>(result);
            return new Response<AccountDto>(account);
        }

        public async Task<Response<bool>> Update(AccountDto account)
        {
            var accountMap = _mapper.Map<Account>(account);
            await _repository.UpdateAsync(accountMap);
            return new Response<bool>(true);
        }
    }
}

