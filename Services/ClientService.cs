﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Application.DTOs;
using Application.Exceptions;
using Application.Interfaces.Repositories;
using Application.Interfaces.Services;
using Application.Wrappers;
using AutoMapper;
using Domain.Entities;
using System.Security.Cryptography;

namespace Services
{
    public class ClientService : IClientService
    {
        private readonly IClientRepository _repository;
        private readonly IMapper _mapper;

        public ClientService(IClientRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<Response<ClientDto>> Create(ClientDto client)
        {
            client.Password = EncryptPassword(client.Password);
            var clientMap = _mapper.Map<Client>(client);
            var result = await _repository.AddAsync(clientMap);
            var clientCreated = _mapper.Map<ClientDto>(result);
            return new Response<ClientDto>(clientCreated);
        }

        public async Task<Response<bool>> Delete(int id)
        {
            var client = await _repository.GetByIdAsync(id);
            await _repository.DeleteAsync(client);
            return new Response<bool>(true);
        }

        public async Task<Response<IEnumerable<ClientDto>>> Get()
        {
            var result = await _repository.ListAsync();
            var clients = _mapper.Map<IEnumerable<ClientDto>>(result);
            return new Response<IEnumerable<ClientDto>>(clients);
        }

        public async Task<Response<ClientDto>> GetById(int id)
        {
            var result = await _repository.GetByIdAsync(id);
            var client = _mapper.Map<ClientDto>(result);
            if(client is null)
            {
                throw new ClientNotFoundException(id.ToString());
            }
            return new Response<ClientDto>(client);
        }

        public async Task<Response<bool>> Update(ClientDto client)
        {
            var clientMap = _mapper.Map<Client>(client);
            await _repository.UpdateAsync(clientMap);
            return new Response<bool>(true);
        }

        private string EncryptPassword(string password)
        {
            using (SHA256 sha256 = SHA256.Create())
            {
                byte[] passwordBytes = Encoding.UTF8.GetBytes(password);
                byte[] hashBytes = sha256.ComputeHash(passwordBytes);

                // Convierte el hash en una cadena hexadecimal
                StringBuilder stringBuilder = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    stringBuilder.Append(hashBytes[i].ToString("x2"));
                }

                return stringBuilder.ToString();
            }
        }
    }
}

