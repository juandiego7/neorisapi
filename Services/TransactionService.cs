﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Application.DTOs;
using Application.Exceptions;
using Application.Interfaces.Repositories;
using Application.Interfaces.Services;
using Application.Wrappers;
using AutoMapper;
using Domain.Entities;

namespace Services
{
    public class TransactionService : ITransactionService
    {
        private readonly ITransactionRepository _repository;
        private readonly IMapper _mapper;

        public TransactionService(ITransactionRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<Response<TransactionDto>> Create(TransactionDto transaction)
        {
            var transactions = await _repository.ListAsync();
            var lastTransaction = transactions.Where(t => t.AccountId == transaction.AccountId)
                                              .ToList()
                                              .OrderByDescending(t => t.Created)
                                              .FirstOrDefault();
            var currentBalance = 0;

            if (lastTransaction != null)
            {
                currentBalance = lastTransaction.Balance;
            }
            else
            {
                var initialBalance = _repository.GetInitialBalance(transaction.AccountId);
                currentBalance = initialBalance;
            }

            if (transaction.Value >= 0)
            {        
                transaction.Type = "suma";
            }
            else
            {
                if(currentBalance + transaction.Value < 0)
                {
                    throw new UnavailableBalanceException();
                }
                transaction.Type = "resta";
            }
            transaction.Balance = currentBalance + transaction.Value;

            var transactionMap = _mapper.Map<Transaction>(transaction);
            var result = await _repository.AddAsync(transactionMap);
            var transactionCreated = _mapper.Map<TransactionDto>(result);
            return new Response<TransactionDto>(transactionCreated);
        }

        public async Task<Response<bool>> Delete(int id)
        {
            var transaction = await _repository.GetByIdAsync(id);
            await _repository.DeleteAsync(transaction);
            return new Response<bool>(true);
        }

        public async Task<Response<IEnumerable<TransactionDto>>> Get()
        {
            var result = await _repository.ListAsync();
            var transactions = _mapper.Map<IEnumerable<TransactionDto>>(result);
            return new Response<IEnumerable<TransactionDto>>(transactions);
        }

        public async Task<Response<TransactionDto>> GetById(int id)
        {
            var result = await _repository.GetByIdAsync(id);
            var transaction = _mapper.Map<TransactionDto>(result);
            return new Response<TransactionDto>(transaction);
        }

        public Response<IEnumerable<ReportDto>> GetReport(string startDate, string endDate, int clientId)
        {
            string format = "dd-MM-yyyy";

            DateTime.TryParseExact(startDate, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime start);
            DateTime.TryParseExact(endDate, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime end);
            end = new DateTime(end.Year, end.Month, end.Day, 23, 59, 59);


            var transactions = _repository.GetReport(start, end, clientId);
            var report = _mapper.Map<IEnumerable<TransactionDto>>(transactions);
            var reportDto = report.Select(r => new ReportDto
            {
                Date = r.Created.ToString("dd/MM/yyyy"),
                Client = r.Account.Client.Name,
                AccountNumber = r.Account.Number,
                AccountType = r.Account.Type,
                InitialBalance = r.Account.InitialBalance.ToString(),
                Status = r.Account.Status,
                TransactValue = r.Value.ToString(),
                AvailableBalance = r.Balance.ToString()
            });
            return new Response<IEnumerable<ReportDto>>(reportDto);
        }

        public async Task<Response<bool>> Update(TransactionDto transaction)
        {
            var transactionMap = _mapper.Map<Transaction>(transaction);
            await _repository.UpdateAsync(transactionMap);
            return new Response<bool>(true);
        }
    }
}

