﻿using System;
using Application.DTOs;
using Application.Wrappers;
using Microsoft.AspNetCore.Mvc.Testing;
using NeorisAPI;
using Newtonsoft.Json;
using System.Net;
using System.Net.Http;
using System.Text;
using Xunit;
using FluentAssertions;

namespace NoerisAPI.Tests.Controllers
{
	public class AccountsControllerTests : IClassFixture<WebApplicationFactory<Startup>>
    {
        private readonly WebApplicationFactory<Startup> _factory;
        private readonly HttpClient _client;

        public AccountsControllerTests(WebApplicationFactory<Startup> factory)
        {
            _factory = factory;
            _client = factory.CreateClient();
        }

        [Fact]
        public void PostAccountSuccess()
        {
            var newClient = new
            {
                Name = "Jose Lema",
                Address = "Otavalo sn y principal",
                Telephone = "098254785",
                Password = "1234",
                Status = "True"
            };

            var content = new StringContent(JsonConvert.SerializeObject(newClient), Encoding.UTF8, "application/json");
            var resultPost = this._client.PostAsync("/api/v1/clientes", content).Result;
            resultPost.EnsureSuccessStatusCode();
            var stringContent = resultPost.Content.ReadAsStringAsync().Result;
            var responsePost = JsonConvert.DeserializeObject<Response<ClientDto>>(stringContent);
            var idClient = responsePost.Result.Id;

            var newAccount = new
            {
                ClientId = idClient,
                Number = "478758",
                Type = "Ahorro",
                InitialBalance = 2000,
                Status = "True"
            };

            content = new StringContent(JsonConvert.SerializeObject(newAccount), Encoding.UTF8, "application/json");
            resultPost = this._client.PostAsync("/api/v1/cuentas", content).Result;
            resultPost.EnsureSuccessStatusCode();
            stringContent = resultPost.Content.ReadAsStringAsync().Result;
            var responseAccountPost = JsonConvert.DeserializeObject<Response<AccountDto>>(stringContent);
            var idAccount = responseAccountPost.Result.Id;

            var resultGet = this._client.GetAsync($"api/v1/cuentas/{idAccount}").Result;
            resultGet.EnsureSuccessStatusCode();
            stringContent = resultGet.Content.ReadAsStringAsync().Result;
            var responseGet = JsonConvert.DeserializeObject<Response<AccountDto>>(stringContent);
            var account = responseGet.Result;

            account.ClientId.Should().Be(newAccount.ClientId);
            account.Number.Should().Be(newAccount.Number);
        }
    }
}

