﻿using System;
using Application.DTOs;
using Application.Wrappers;
using Microsoft.AspNetCore.Mvc.Testing;
using NeorisAPI;
using Newtonsoft.Json;
using System.Net;
using System.Net.Http;
using System.Text;
using Xunit;
using FluentAssertions;
using Application.Exceptions;

namespace NoerisAPI.Tests.Controllers
{
	public class ClientsControllerTests : IClassFixture<WebApplicationFactory<Startup>>    
	{

        private readonly WebApplicationFactory<Startup> _factory;
        private readonly HttpClient _client;

        public ClientsControllerTests(WebApplicationFactory<Startup> factory)
        {
            _factory = factory;
            _client = factory.CreateClient();
        }

        [Fact]
        public void PostClientSuccess()
        {
            var newClient = new
            {
                Name = "Jose Lema",
                Address = "Otavalo sn y principal",
                Telephone = "098254785",
                Password = "1234",
                Status = "True"
            };
            var content = new StringContent(JsonConvert.SerializeObject(newClient), Encoding.UTF8, "application/json");
            var resultPost = this._client.PostAsync("/api/v1/clientes", content).Result;
            resultPost.EnsureSuccessStatusCode();
            var stringContent = resultPost.Content.ReadAsStringAsync().Result;
            var responsePost = JsonConvert.DeserializeObject<Response<ClientDto>>(stringContent);
            var idClient = responsePost.Result.Id;

            var resultGet = this._client.GetAsync($"api/v1/clientes/{idClient}").Result;
            resultGet.EnsureSuccessStatusCode();
            stringContent = resultGet.Content.ReadAsStringAsync().Result;
            var responseGet = JsonConvert.DeserializeObject<Response<ClientDto>>(stringContent);
            var client = responseGet.Result;

            client.Name.Should().Be(newClient.Name);
            client.Address.Should().Be(newClient.Address);
        }

        [Fact]
        public void GetClientError()
        {
            Random random = new Random();
            int randomNumber = random.Next(1, 101);
            int idClient = randomNumber;
            HttpResponseMessage response = null;
            try
            {
                response = this._client.GetAsync($"api/v1/clients/{idClient}").Result;
                response.EnsureSuccessStatusCode();
                Assert.True(false, "Deberia fallar");
            }
            catch (Exception)
            {
                response.StatusCode.Should().Be(HttpStatusCode.NotFound);
            }
        }
    }
}

