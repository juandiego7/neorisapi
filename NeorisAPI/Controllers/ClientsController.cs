﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.DTOs;
using Application.Interfaces.Services;
using Application.Wrappers;
using Microsoft.AspNetCore.Mvc;

namespace NeorisAPI.Controllers
{
    [ApiVersion("1.0")]
    [ApiController]
    [Route("api/v{version:apiVersion}/clientes")]
    public class ClientsController : ControllerBase
    {
        private readonly IClientService _service;
        public ClientsController(IClientService service)
        {
            _service = service;
        }

        [HttpGet("{id}")]
        public async Task<Response<ClientDto>> Get(int id)
        {
            return await _service.GetById(id);
        }

        [HttpGet]
        public async Task<Response<IEnumerable<ClientDto>>> Get()
        {
            return await _service.Get();
        }

        [HttpPost]
        public async Task<Response<ClientDto>> Post(ClientDto client)
        {
            return await _service.Create(client);
        }

        [HttpPut]
        public async Task<Response<bool>> Put(ClientDto client)
        {
            return await _service.Update(client);
        }

        [HttpDelete("{id}")]
        public async Task<Response<bool>> Delete(int id)
        {
            return await _service.Delete(id);
        }
    }
}