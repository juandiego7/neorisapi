﻿using System;
using Microsoft.AspNetCore.Builder;
using NeorisAPI.Middlewares;

namespace NeorisAPI.Extensions
{
	public static class AppExtensions
	{
		public static void UseErrorHandlingMiddleware(this IApplicationBuilder app)
		{
			app.UseMiddleware<ErrorHandlerMiddleware>();
		}
	}
}

